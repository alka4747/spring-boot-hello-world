FROM maven:3.6.3-openjdk-11 As builder
WORKDIR /app
COPY . .
RUN mvn clean package

FROM openjdk:11.0.8-jre-slim as prod
# default to port 8080
ARG PORT=8080
ENV PORT $PORT
EXPOSE $PORT
# Add non root user named Java
RUN groupadd --gid 1000 java \
  && useradd --uid 1000 --gid java --shell /bin/bash --create-home java
USER java
WORKDIR /app
COPY --from=builder --chown=java:java /app/target/*helloworld*.jar /app/helloworld.jar
CMD ["java", "-jar", "./helloworld.jar"]