# Spring Boot Hello World

A simple dockerized spring boot app

## Build

In order to create an uber jar of the app, run the following command:

```bash
./mvnw clean package -DskipTests
```

In order to build the app inside a Docker image, run the following command from the root directory (Docker is required):

```bash
docker build -t spring_boot_hello_world .
```

## Run

In order to run the app, run the following command from the root directory (Java 11 is required):

```bash
java -jar ./target/*hello-world*.jar
```

In order to run the app as a container, run the following command:

```bash
docker run -d -p 9090:8080 spring_boot_hello_world
```

## Deploy on Minikube

In order to deploy the app on an existing minikube cluster, run the following commands from the root directory:

```bash
kubectl apply -f k8s/deployment.yaml
kubectl apply -f k8s/service.yaml
```

You may verify the deployment using the following commands:

```bash
export PORT=$(kubectl get svc spring-boot-hello-world-service -o go-template='{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}')
curl host01:$PORT
```