package com.example.helloworld;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloWorldController {
    private int counter = 0;
    @RequestMapping("/")
    public String index() {
        this.counter++;
        return "hello-world-" + this.counter;
    }

}
